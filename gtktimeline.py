#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Pitivi video editor
#
# Copyright (c) 2014 Thibault Saunier <tsaunier@gnome.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA 02110-1301, USA.

from gi.repository import Gst
from gi.repository import GES
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Pango
from gi.repository import GObject

PLAYHEAD_WIDTH = 1
PLAYHEAD_COLOR = (200, 0, 0)

class Timeline(Gtk.Box):
    def __init__(self, bTimeline):
        super(Timeline, self).__init__(orientation=Gtk.Orientation.VERTICAL)

        self.bTimeline = bTimeline
        self.bTimeline.connect("layer-added", self._layerAddedCb)

        self._setUpDragAndDrop()

    def do_draw(self, cr):
        #print("%s DRAW" % self)
        Gtk.Box.do_draw(self, cr)
        self.drawPosition(cr)

    def drawPosition(self, context):
        print("Drawing position")
        # Add 0.5 so that the line center is at the middle of the pixel,
        # without this the line appears blurry.
        xpos = 70 + 25
        context.set_line_width(PLAYHEAD_WIDTH + 2)
        context.set_source_rgb(*PLAYHEAD_COLOR)
        context.move_to(xpos, 0)
        context.line_to(xpos, context.get_target().get_height())
        context.stroke()

        print("=> Done drawing")

    def _dragMotionCb(self, widget, context, x, y, timestamp):
        pass
        #print("MOTION")

    def _dragDataReceivedCb(self, widget, drag_context, unused_x, unused_y, selection_data, unused_info, timestamp):
        pass
        #print("Data received")

    def _dragDropCb(self, widget, context, x, y, timestamp):
        pass
        #print("Dropped")

    def _dragLeaveCb(self, widget, unused_context, unused_timestamp):
        pass
        #print("TIMELINE LEAVE")

    def _eventCb(self, widget, event):

        return True

    def _setUpDragAndDrop(self):
        self.connect('drag-motion', self._dragMotionCb)
        self.connect('drag-data-received', self._dragDataReceivedCb)
        self.connect('drag-drop', self._dragDropCb)
        self.connect('drag-leave', self._dragLeaveCb)
        self.connect('event', self._eventCb)

    def _layerAddedCb(self, timeline, layer):
        self.add_layer(layer)

    def add_layer(self, bLayer):
        self.pack_start(Layer(bLayer), True, True, 10)

class Layer(Gtk.Layout):
    def __init__(self, bLayer):
        super(Layer, self).__init__()

        print(self.get_hadjustment())
        self.bLayer = bLayer

        label = Gtk.Label(label="Layer: %d" % bLayer.get_priority())
        self.put(label, 0, 0)
        #self.connect ("button-press-event", self._eventCb)
        self.bLayer.connect("clip-added", self._clipAddedCb)

    def _eventCb(self, widget, event):
        print("LAYER EVENT %s" % event)

        return True

    def _clipAddedCb(self, layer, clip):
        # FIXME We should always shift all the timeline content
        # to the size of the layer control widget, here 70px
        self.put(Clip(self, clip), 70 + clip.props.start, 0)

    def do_draw(self, cr):
        print("Drawing layer")
        Gtk.Layout.do_draw(self, cr)
        print("===> DONE Drawing layer")
        self.drawPosition(cr)

    def drawPosition(self, context):
        print("Drawing LAYER position")
        # Add 0.5 so that the line center is at the middle of the pixel,
        # without this the line appears blurry.
        xpos = 70 + 25
        context.set_line_width(PLAYHEAD_WIDTH + 2)
        context.set_source_rgb(*PLAYHEAD_COLOR)
        context.move_to(xpos, 0)
        context.line_to(xpos, context.get_target().get_height())
        context.stroke()
        print("==+> DONE Drawing LAYER position")

class TimelineElement(Gtk.Box):
    def __init__ (self, element, backgroud_color):
        super (TimelineElement, self).__init__()

        self.bElement = element
        self.override_background_color (Gtk.StateFlags.NORMAL, backgroud_color)

        label = Gtk.Label (None)
        label.set_markup("<span foreground='blue'>%s</span>" % self.bElement.get_name())
        label.set_ellipsize(Pango.EllipsizeMode.END)
        self.add(label)


class VideoSource(TimelineElement):
    def __init__(self, element):
        backgroud_color = Gdk.RGBA()
        backgroud_color.red = 0.0
        backgroud_color.blue = 1.0
        backgroud_color.green = 1.0
        backgroud_color.alpha = 1.0

        super(VideoSource, self).__init__(element, backgroud_color)


class AudioSource(TimelineElement):
    def __init__(self, element):
        backgroud_color = Gdk.RGBA()
        backgroud_color.red = 0.0
        backgroud_color.blue = 0.0
        backgroud_color.green = 1.0
        backgroud_color.alpha = 1.0

        super(AudioSource, self).__init__(element, backgroud_color)


class Clip(Gtk.EventBox):
    def __init__(self, layer, bClip):
        super (Clip, self).__init__()

        self._drawing = True
        self._handle_position_factor = 2/3
        self.layer = layer
        self.bClip = bClip

        self._audioSource = None
        self._videoSource = None

        self._setupWidget()
        self._connectWidgetSignals()

        self._connectGES()

    def _setupWidget(self):
        self._paned = Gtk.Paned.new(Gtk.Orientation.VERTICAL)
        self.add(self._paned)

        color = Gdk.RGBA()
        color.parse("rgba(100,90,80,0.1)")
        self.override_background_color (Gtk.StateFlags.NORMAL, color)

        for child in self.bClip.get_children(False):
            self._childAdded(self.bClip, child)

    def _vpanedHandlePositionCb(self, widget, unused_pspec):
        parent_height = self.get_parent().get_allocated_height()

        if self._drawing:
            return

        print("====> Moved %s => %f" % (widget.props.position, widget.props.position / parent_height))
        self._handle_position_factor = widget.props.position / parent_height

    def do_draw(self, cr):
        self._drawing = True
        parent_height = self.get_parent().get_allocated_height()
        #print("%s Druation %s" % (self.bClip.get_name(), self.bClip.props.duration))
        self.set_size_request(self.bClip.props.duration, parent_height)

        self._paned.set_position(parent_height * self._handle_position_factor)

        Gtk.EventBox.do_draw(self, cr)
        self._drawing = False
        #print("Result %s" % self.get_allocated_width())

    def _connectWidgetSignals(self):
        self.connect ("button-press-event", self.clickedCb)

        self._paned.connect("notify::position", self._vpanedHandlePositionCb)

    def clickedCb (self, element, event):
        #print("CLICKED! %s" % self.bClip.props.start)
        v = GObject.Value()
        v.init(GObject.TYPE_INT)

        self.bClip.props.start += 1
        self.layer.move (element, 70 + self.bClip.props.start, 0)
        #print (element, event)

    def _childAdded(self, clip, child):
        if isinstance(child, GES.Source):
            #print("%s added %s" % (child, child.get_track_type()))
            self._drawing = True
            if child.get_track_type() == GES.TrackType.AUDIO:
                assert(self._audioSource is None)
                self._audioSource = AudioSource(child)
                self._paned.pack2(self._audioSource, True, False)
            elif child.get_track_type() == GES.TrackType.VIDEO:
                self._videoSource = VideoSource(child)
                self._paned.pack1(self._videoSource, True, False)
            self._drawing = False

    def _childAddedCb(self, clip, child):
        self._childAdded(clip, child)

    def _connectGES(self):
        self.bClip.connect("child-added", self._childAddedCb)


Gst.init(None)
GES.init()

gtksettings = Gtk.Settings.get_default()
gtksettings.set_property("gtk-application-prefer-dark-theme", True)

window = Gtk.Window()
window.set_default_size(800, 400)
window.connect("destroy", lambda q: Gtk.main_quit())

bTimeline = GES.Timeline.new_audio_video()
timeline = Timeline(bTimeline)

bTimeline.append_layer()
bTimeline.append_layer()

clip = GES.TestClip.new()
clip.props.start = 0
clip.props.duration = 100
bTimeline.get_layers()[0].add_clip(clip)

clip = GES.TestClip.new()
clip.props.start = 100
clip.props.duration = 100
bTimeline.get_layers()[0].add_clip(clip)

clip = GES.TestClip.new()
clip.props.start = 0
clip.props.duration = 100
bTimeline.get_layers()[1].add_clip(clip)

label2 = Gtk.Label(label="Label-2")

window.add(timeline)
window.show_all()

Gtk.main()

